package pages2;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistroHospitalizacion {
	private WebDriver driver;
	private int maxWait = 3000;
	
	public RegistroHospitalizacion(WebDriver driver) {
		this.driver = driver;
	}
	
	public void registroHospitalizacion(String nhc) {
		WebDriverWait waitBusqueda = new WebDriverWait(driver, 9000);
		waitBusqueda.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='zk_comp_133']")));
		
		driver.findElement(By.xpath("//*[@id='zk_comp_131']")).sendKeys(nhc);
		driver.findElement(By.xpath("//*[@id='zk_comp_133']")).click();
		
		try {
			Thread.sleep(maxWait);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.findElement(By.xpath("//*[@id='zk_comp_304-btn']")).click();
		driver.findElement(By.xpath("//*[@id='zk_comp_1017']/td[2]")).click();
		
		driver.findElement(By.xpath("//*[@id='zk_comp_344']")).sendKeys("Dolor de panza");
		
		driver.findElement(By.xpath("//*[@id='zk_comp_379-btn']")).click();
		try {
			Thread.sleep(maxWait);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		driver.findElement(By.xpath("//*[@id='zk_comp_993']")).click(); // El click va al td
		
		try {
			Thread.sleep(maxWait);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		driver.findElement(By.xpath("//*[@id='zk_comp_393-btn']")).click();// 
		try {
			Thread.sleep(maxWait);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//*[@id='zk_comp_935']")).click(); 
		
		driver.findElement(By.xpath("//*[@id='zk_comp_532-btn']")).click();
		try {
			Thread.sleep(maxWait);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//*[@id='zk_comp_985']")).click();
		
		driver.findElement(By.xpath("//*[@id='zk_comp_909']")).click(); // REVISAR ESTO (�APARECE SIEMPRE O NO?)
	}
}
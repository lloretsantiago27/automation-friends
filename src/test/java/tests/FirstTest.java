spackage tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import datos.DataProvider;
import pages.Busqueda;
import pages.CrearPaciente;
import pages.LogIn;
import pages.NuevoPaciente;
import pages.Portada;

@RunWith(Parameterized.class)
public class FirstTest {
	
	public String myUser = "administrativo.st-es";
	public String myPass = "administrativo.st-es";
	
	@Parameterized.Parameter(0)
	public JSONObject jsonObject;
	
	@Parameterized.Parameters
	public static Collection datos() throws FileNotFoundException, IOException, ParseException {
		return DataProvider.getDataByJson();
	}
	
	//Instancia del driver
	public static WebDriver driver;
	
	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver","src/drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@AfterClass
	public static void tearDownClass() {
		driver.close();
		driver.quit();
	}
	
	@Before
	public void before() {
		driver.manage().window().maximize();
		driver.navigate().to("http://10.115.88.117:8080/ehHIS-ui");
	}
	
	@After
	public void tearDown() {
		//driver.quit();
	}
	
	@Test
	public void testIngresoPMG(){
		
		LogIn myLogin = new LogIn(driver);
		myLogin.logIn(myUser, myPass);
		
		String primerApellido = (String) jsonObject.get("primerApellido");
		String segundoApellido = (String) jsonObject.get("segundoApellido");
		String nombre = (String) jsonObject.get("nombre");
		String sexo = (String) jsonObject.get("sexo");
	
		Portada myPortada = new Portada(driver);
		myPortada.portada();

		Busqueda myBusqueda = new Busqueda(driver);
		myBusqueda.busqueda(primerApellido, segundoApellido, nombre, sexo);
		
		CrearPaciente myCrearPaciente = new CrearPaciente(driver);
		myCrearPaciente.crearPaciente();
		
		NuevoPaciente myNuevoPaciente = new NuevoPaciente(driver);
		myNuevoPaciente.nuevoPaciente();
	}
}
package tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.LogIn;
import pages.Portada;
import pages2.RegistroHospitalizacion;

public class SecondTest {
	
	private static WebDriver driver;
	public static String myUser = "administrativo.st-es";
	public static String myPass = "administrativo.st-es";
	public static String myNhc = "11000003"; //  11000215-> PATEST
	
	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver","src/drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@AfterClass
	public static void afterClass() {
		//driver.close();
		//driver.quit();
	}
	
	@Before
	public void before() {
		driver.manage().window().maximize();
		driver.get("http://10.115.88.117:8080/ehHIS-ui");
	}
	
	@After()
	public void after() {
		
	}
	
	@Test
	public void testRegistroHospi() {
		LogIn myLogIn = new LogIn(driver);
		myLogIn.logIn(myUser, myPass);
		
		Portada myPortada = new Portada(driver);
		myPortada.portada();
		
		RegistroHospitalizacion myReg = new RegistroHospitalizacion(driver);
		myReg.registroHospitalizacion(myNhc);
	}
}

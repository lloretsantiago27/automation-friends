package datos;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DataProvider {

	public static Collection getDataByJson() throws FileNotFoundException, IOException, ParseException {
		ArrayList<Object[]> data = new ArrayList<Object[]>();
		JSONParser convertidor = new JSONParser();
		JSONObject lineaJson = (JSONObject) convertidor.parse(new FileReader("src/datosPacientes.json")); //Fijarse si parte del src
		Object[] claves = lineaJson.keySet().toArray();
		for(Object clave : claves) {
			JSONObject json = (JSONObject) lineaJson.get(clave);
			data.add(new Object[]{json});
		}
		return data;
	}
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Portada {

	private  WebDriver driver;
	
	public Portada(WebDriver driver) {
		this.driver = driver;
	}
	
	public void portada() {
		driver.findElement(By.xpath("//*[@id='zk_comp_34']")).click();
	}
}

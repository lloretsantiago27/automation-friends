package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NuevoPaciente {
	private WebDriver driver;
	
	public NuevoPaciente(WebDriver driver) {
		this.driver = driver;
	}
	
	public void nuevoPaciente() {
		
		WebDriverWait waitCheckSec = new WebDriverWait(driver, 1000);
		waitCheckSec.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='zk_comp_1168-real']")));
		
		driver.findElement(By.xpath("//*[@id='zk_comp_1168-real']")).click();
		
		driver.findElement(By.xpath("//*[@id='zk_comp_1242-real']")).sendKeys("11/11/1991"); 
		driver.findElement(By.xpath("//*[@id='zk_comp_1254-real']")).sendKeys("Desconocido - DESC");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//*[@id='zk_comp_1254-real']")).sendKeys(Keys.ARROW_DOWN); 
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//*[@id='zk_comp_1254-real']")).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//*[@id='zk_comp_2041']")).click();
	}
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Busqueda {
	private  WebDriver driver;
	
	public Busqueda(WebDriver driver) {
		this.driver = driver;
	}
	
	public void busqueda(String pApellido, String sApellido, String nombre, String sexo) {
		
		WebDriverWait waitBusquedaAva = new WebDriverWait(driver, 9000);
		waitBusquedaAva.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='zk_comp_134']")));
		
		driver.findElement(By.xpath("//*[@id='zk_comp_135']")).click();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		driver.findElement(By.xpath("//*[@id='zk_comp_1012']")).sendKeys(pApellido);
		//driver.findElement(By.xpath("//*[@id='zk_comp_1016']")).sendKeys(sApellido);
		driver.findElement(By.xpath("//*[@id='zk_comp_1020']")).sendKeys(nombre);
		driver.findElement(By.xpath("//*[@id='zk_comp_1024-real']")).sendKeys(sexo);
		
		WebDriverWait waitBusqueda1 = new WebDriverWait(driver, 9000); //REVISAR
		waitBusqueda1.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='zk_comp_1060']")));
		
		try {
			driver.findElement(By.xpath("//*[@id='zk_comp_1060-chdex']")).click();
		}catch(ElementClickInterceptedException e) {
			driver.findElement(By.xpath("//*[@id='zk_comp_1060']")).click();
		}
	}
}

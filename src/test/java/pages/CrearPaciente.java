package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CrearPaciente {
	private WebDriver driver;
	
	public CrearPaciente(WebDriver driver) {
		this.driver = driver;
	}
	
	public void crearPaciente() {
		WebDriverWait waitBotonCrear = new WebDriverWait(driver,1000);
		waitBotonCrear.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='zk_comp_1093']")));
		
		driver.findElement(By.xpath("//*[@id='zk_comp_1093']")).click();
		
		try {
			Thread.sleep(9000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
